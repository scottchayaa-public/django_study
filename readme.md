# Django demo01


```sh
python -m venv venv

source venv/Scripts/activate

pip install django

....

# 匯出依賴套件
pip freeze > requirements.txt

# 安裝依賴套件
pip install -r requirements.txt
```

# References
 - https://docs.djangoproject.com/en/3.1/intro/tutorial01/
 - https://www.django-rest-framework.org/tutorial/quickstart/